class Dictionary
{
    Record[] records;
    int size;
    int length;

    Dictionary(int s)
    {
        size=s;
        length=0;
        records = new Record[size];
    }
    public double get(int key)
    {
        if(isEmpty())
        {
            return 0.0;
        }
        for(int i=0; i<length; i++)
        {
            if(records[i].getKey()==key)
            {
                return records[i].getValue();
            }
        }
        return 0.0;
    }
    public Record put(Record record)
    {
        if(isFull())
        {
            System.out.println("[AUTHOR] Uh oh! This Dictionary isn't accepting records");
            return null;
        }
        for(int i=0; i<length; i++)
        {
            if(records[i].getKey()==record.getKey())
            {
                Record oldRecordAtKey=records[i];
                records[i]=record;
                return oldRecordAtKey;
            }
        }
        records[length]=record;
        length++;
        return null;
    }
    public Record remove(int key)
    {
        if(isEmpty())
        {
            System.out.println("[AUTHOR] Uh oh! This Dictionary is empty.");
            return null;
        }
        int index=-1;
        for(int i=0; i<length; i++)
        {
            if(records[i].getKey()==key)
            {
                index=i;
            }
        }
        if(index==-1)
        {
            return null;
        }
        else
        {
            Record RecordAtKey=records[index];
            for(int i=index; i<length-1; i++)
            {
                records[i]=records[i+1];
            }
            records[length-1]=new Record(0.0);
            length--;
            return RecordAtKey;
        }
    }
    public int size()
    {
        return size;
    }
    public boolean isEmpty()
    {
        return length==0;
    }
    public boolean isFull()
    {
        return length==size;
    }
    public void records()
    {
        for(int i=0; i<length; i++)
        {
            System.out.println(records[i].getValue() + " ");
        }
    }
    public void keys()
    {
        for(int i=0; i<length; i++)
        {
            System.out.println(records[i].getKey() + " ");
        }
    }
}