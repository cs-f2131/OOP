public class My_Class
{
    public static void main(String[] args)
    {
        Dictionary myDictionary = new Dictionary(5);
        System.out.println("Empty?: " + myDictionary.isEmpty());
        System.out.println("Full?: " + myDictionary.isFull());
        myDictionary.put(new Record(1.6));
        myDictionary.put(new Record(2.5));
        myDictionary.put(new Record(3.2));
        System.out.println("Added 1.6, 2.5, 3.8");
        System.out.println("Empty?: " + myDictionary.isEmpty());
        System.out.println("Full?: " + myDictionary.isFull());
        System.out.print("Showing records: ");
        myDictionary.records();
        System.out.print("Showing keys: ");
        myDictionary.keys();
        myDictionary.put(new Record(4.6));
        myDictionary.put(new Record(5.5));
        myDictionary.put(new Record(7.2));
        System.out.println("Added 4.6, 5.5, 7.2");
        System.out.println("Empty?: " + myDictionary.isEmpty());
        System.out.println("Full?: " + myDictionary.isFull());
        System.out.print("Showing records: ");
        myDictionary.records();
        System.out.print("Showing keys: ");
        myDictionary.keys();
    }
}
